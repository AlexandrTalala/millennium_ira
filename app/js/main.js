$(document).ready(function () {
  /*-------------------------------------
   Preloader
   -------------------------------------*/
  $(window).on('load', function () {
    // var preloader = $('.loader');
    // if (preloader.length) {
    //   preloader.delay(600).fadeOut('slow');
    //
    //
    // }

    wow.init();


    if ($('.split').length) {
      setTimeout(function () {
        $('.split').addClass('active');
      }, 1000);
    }
  });

  /*-------------------------------------
   Burger Menu
   -------------------------------------*/
  $('.humburger').on('click', function () {
    $('.menu').toggleClass('active');
    $(this).toggleClass('active');
    $('body').toggleClass('body--fixed');
  });

  /*-------------------------------------
   Menu links
   -------------------------------------*/
  $('.menu_icon').on('click', function () {
    $('.menu_submenu').not(':hidden').slideUp();
    $(this).next('.menu_submenu').not(':visible').slideToggle();
    $('.menu_icon').not(this).removeClass('active')
    $(this).toggleClass('active');
  });

  /*-------------------------------------
   Text Split
   -------------------------------------*/
  function textSplit(target) {
    target.addBack().contents().each(function () {
      if (this.nodeType == 3) {
        $(this).replaceWith($(this).text().replace(/(\w)/g, "<span>$&</span>"));
      }
    });
  }

  $(function () {
    textSplit($('.split'));
  });

  /*-------------------------------------
   Wow Animate
   -------------------------------------*/
  var wow = new WOW(
    {
      boxClass: 'wow',
      animateClass: 'animated',
      offset: 0,
      mobile: true,
      live: true
    }
  );


  $('.feedback_slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    // prevArrow: '<i class="icon-chevrons-left" aria-label="Previous"></i>',
    // nextArrow: '<i class="icon-chevrons-right" aria-label="Next"></i>',

  });


});